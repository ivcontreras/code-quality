'use strict';

// This class is used for logins
class Login {
  constructor(users) {
    this.sessions = [];
    this.users = users;
  }

  logout(user) {
    let index = this.idx(user, this.sessions);
    if(index > -1) {
        this.sessions.splice(index, 1);
        return true;
    }

    return false;
  }

  // Checks if user exists
  userExists(user) {
    for (let u of this.users) {
        if (u.user === user.user && u.pass === user.pass) {
            return true;
        }
    }
    
    return false;
  }

  // Register user
  registerUser(user) {
    let index = this.idx(user, this.users);

    if(index === -1) {
        this.users.push(user);
        return true;
    }
    
    return false;
  }

  removeUser(user) {
    let index = this.idx(user, this.users);
    this.users[index] = null;
    this.passwords[index] = null;
    this.users = this.users.filter(user => user !== null);
    this.passwords = this.passwords.filter(password => password !== null);
  }

  checkPassword(user, password) {
    let index = this.idx(user, this.users);
    let passwordCorrect = this.passwords[index] === password;
    return passwordCorrect;
  }

  updatePassword(user, newPassword) {    
    let index = this.idx(user, this.users);

    if(index > -1){
        this.users[index].pass = newPassword;
        return true;
    }

    return false;
  }

  login(user) {
    let index = this.idx(user, this.users);

    if(index > -1){ 
        this.sessions.push(user);
        return true;
    }

    return false;
  }

  // Gets index of an element in an array
  idx(user, array) {
    let cont = 0;
    for (let u of array) {
        if(u.user === user.user && u.pass === user.pass) {
            return cont;
        }
        cont += 1;
    }

    return -1;
  }
}

let registeredUsers = [
    {user: 'user1', pass: 'pass1'},
    {user: 'user2', pass: 'pass2'},
    {user: 'user3', pass: 'pass3'}
];

let login = new Login(registeredUsers);

login.registerUser({user: 'user4', pass: 'pass4'});
login.login({user: 'user4', pass: 'pass4'});
login.updatePassword({user: 'user3', pass: 'pass3'}, 'pass5');
login.login({user: 'user3', pass: 'pass5'});
login.logout('user4');
login.logout('user3');